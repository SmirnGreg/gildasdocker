# GILDAS docker image

The docker image for [GILDAS](https://www.iram.fr/IRAMFR/GILDAS/).

### Download:

```bash
docker pull smirngreg/gildas  # for latest version
docker pull smirngreg/gildas:jun20b  # for PRODIGE-default jun20b version
```

### Run:

```bash
docker run \
  -it \  # interactive, TTY terminal
  -e DISPLAY=$DISPLAY \  # forward current X display variable
  smirngreg/gildas:jun20b  # docker image name
```

### Build:
(not necessary, takes ~2 hours)

```
git clone https://gitlab.com/SmirnGreg/gildasdocker.git
cd gildasdocker
docker build . -t gildas 
```


Author: Grigorii (Greg) Smirnov-Pinchukov
