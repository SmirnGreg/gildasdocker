FROM ubuntu:20.04 

SHELL ["/bin/bash", "-c"]

ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update && apt install -y \
  gfortran-7\
  python-dev\
  python-numpy\
  make\
  libcfitsio-dev\
  wget\
  bash\
  pkg-config\
  libgtk2.0-dev\
  libpng-dev\
  lpr\
  ghostscript\
  vim nano

RUN g++ --version
RUN gfortran-7 --version
COPY /gildas-src-feb21a /gildas
WORKDIR /gildas
RUN source admin/gildas-env.sh -o openmp -c gfortran-7 && make && make install

ENV GAG_ROOT_DIR=//gildas-exe
ENV GAG_EXEC_SYSTEM=x86_64-ubuntu20.04-gfortran-7-openmp

RUN echo "source $GAG_ROOT_DIR/etc/bash_profile" >> ~/.bashrc
